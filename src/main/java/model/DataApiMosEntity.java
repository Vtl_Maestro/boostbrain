package model;

import javax.persistence.*;
import javax.persistence.Entity;

@Entity
public class DataApiMosEntity {
    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer code;
    private Integer id;
    private Integer versionNumber;
    private Integer releaseNumber;
    private String caption;
    private Integer categoryId;
    private Integer departmentId;
    private String publishDate;
    @Lob
    private String fullDescription;
    @Lob
    private String keywords;
    private Boolean containsGeodata;
    private Boolean containsAccEnvData;
    private Boolean isForeign;
    private Boolean isSeasonal;
    private String season;
    private Boolean isArchive;
    private Boolean isNew;
    private String lastUpdateDate;
    private String sefUrl;
    private String identificationNumber;


    public DataApiMosEntity() {

    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(Integer versionNumber) {
        this.versionNumber = versionNumber;
    }

    public Integer getReleaseNumber() {
        return releaseNumber;
    }

    public void setReleaseNumber(Integer releaseNumber) {
        this.releaseNumber = releaseNumber;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Boolean getContainsGeodata() {
        return containsGeodata;
    }

    public void setContainsGeodata(Boolean containsGeodata) {
        this.containsGeodata = containsGeodata;
    }

    public Boolean getContainsAccEnvData() {
        return containsAccEnvData;
    }

    public void setContainsAccEnvData(Boolean containsAccEnvData) {
        this.containsAccEnvData = containsAccEnvData;
    }

    public Boolean getForeign() {
        return isForeign;
    }

    public void setForeign(Boolean foreign) {
        isForeign = foreign;
    }

    public Boolean getSeasonal() {
        return isSeasonal;
    }

    public void setSeasonal(Boolean seasonal) {
        isSeasonal = seasonal;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public Boolean getArchive() {
        return isArchive;
    }

    public void setArchive(Boolean archive) {
        isArchive = archive;
    }

    public Boolean getNew() {
        return isNew;
    }

    public void setNew(Boolean aNew) {
        isNew = aNew;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getSefUrl() {
        return sefUrl;
    }

    public void setSefUrl(String sefUrl) {
        this.sefUrl = sefUrl;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }
}
