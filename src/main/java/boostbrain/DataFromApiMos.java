package boostbrain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class DataFromApiMos {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("VersionNumber")
    @Expose
    private Integer versionNumber;
    @SerializedName("ReleaseNumber")
    @Expose
    private Integer releaseNumber;
    @SerializedName("Caption")
    @Expose
    private String caption;
    @SerializedName("CategoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("DepartmentId")
    @Expose
    private Integer departmentId;
    @SerializedName("PublishDate")
    @Expose
    private String publishDate;
    @SerializedName("FullDescription")
    @Expose
    private String fullDescription;
    @SerializedName("Keywords")
    @Expose
    private String keywords;
    @SerializedName("ContainsGeodata")
    @Expose
    private Boolean containsGeodata;
    @SerializedName("ContainsAccEnvData")
    @Expose
    private Boolean containsAccEnvData;
    @SerializedName("IsForeign")
    @Expose
    private Boolean isForeign;
    @SerializedName("IsSeasonal")
    @Expose
    private Boolean isSeasonal;
    @SerializedName("Season")
    @Expose
    private String season;
    @SerializedName("IsArchive")
    @Expose
    private Boolean isArchive;
    @SerializedName("IsNew")
    @Expose
    private Boolean isNew;
    @SerializedName("LastUpdateDate")
    @Expose
    private String lastUpdateDate;
    @SerializedName("SefUrl")
    @Expose
    private String sefUrl;
    @SerializedName("IdentificationNumber")
    @Expose
    private String identificationNumber;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(Integer versionNumber) {
        this.versionNumber = versionNumber;
    }

    public Integer getReleaseNumber() {
        return releaseNumber;
    }

    public void setReleaseNumber(Integer releaseNumber) {
        this.releaseNumber = releaseNumber;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Boolean getForeign() {
        return isForeign;
    }

    public void setForeign(Boolean foreign) {
        isForeign = foreign;
    }

    public Boolean getSeasonal() {
        return isSeasonal;
    }

    public void setSeasonal(Boolean seasonal) {
        isSeasonal = seasonal;
    }

    public Boolean getArchive() {
        return isArchive;
    }

    public void setArchive(Boolean archive) {
        isArchive = archive;
    }

    public Boolean getNew() {
        return isNew;
    }

    public void setNew(Boolean aNew) {
        isNew = aNew;
    }

    public Boolean getContainsGeodata() {
        return containsGeodata;
    }

    public void setContainsGeodata(Boolean containsGeodata) {
        this.containsGeodata = containsGeodata;
    }

    public Boolean getContainsAccEnvData() {
        return containsAccEnvData;
    }

    public void setContainsAccEnvData(Boolean containsAccEnvData) {
        this.containsAccEnvData = containsAccEnvData;
    }

    public Boolean getIsForeign() {
        return isForeign;
    }

    public void setIsForeign(Boolean isForeign) {
        this.isForeign = isForeign;
    }

    public Boolean getIsSeasonal() {
        return isSeasonal;
    }

    public void setIsSeasonal(Boolean isSeasonal) {
        this.isSeasonal = isSeasonal;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public Boolean getIsArchive() {
        return isArchive;
    }

    public void setIsArchive(Boolean isArchive) {
        this.isArchive = isArchive;
    }

    public Boolean getIsNew() {
        return isNew;
    }

    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getSefUrl() {
        return sefUrl;
    }

    public void setSefUrl(String sefUrl) {
        this.sefUrl = sefUrl;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataFromApiMos that = (DataFromApiMos) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(versionNumber, that.versionNumber) &&
                Objects.equals(releaseNumber, that.releaseNumber) &&
                Objects.equals(caption, that.caption) &&
                Objects.equals(categoryId, that.categoryId) &&
                Objects.equals(departmentId, that.departmentId) &&
                Objects.equals(publishDate, that.publishDate) &&
                Objects.equals(fullDescription, that.fullDescription) &&
                Objects.equals(keywords, that.keywords) &&
                Objects.equals(containsGeodata, that.containsGeodata) &&
                Objects.equals(containsAccEnvData, that.containsAccEnvData) &&
                Objects.equals(isForeign, that.isForeign) &&
                Objects.equals(isSeasonal, that.isSeasonal) &&
                Objects.equals(season, that.season) &&
                Objects.equals(isArchive, that.isArchive) &&
                Objects.equals(isNew, that.isNew) &&
                Objects.equals(lastUpdateDate, that.lastUpdateDate) &&
                Objects.equals(sefUrl, that.sefUrl) &&
                Objects.equals(identificationNumber, that.identificationNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, versionNumber, releaseNumber, caption, categoryId, departmentId, publishDate, fullDescription, keywords, containsGeodata, containsAccEnvData, isForeign, isSeasonal, season, isArchive, isNew, lastUpdateDate, sefUrl, identificationNumber);
    }

    @Override
    public String toString() {
        return "DataFromApiMos{" +
                "id=" + id +
                ", versionNumber=" + versionNumber +
                ", releaseNumber=" + releaseNumber +
                ", caption='" + caption + '\'' +
                ", categoryId=" + categoryId +
                ", departmentId=" + departmentId +
                ", publishDate='" + publishDate + '\'' +
                ", fullDescription='" + fullDescription + '\'' +
                ", keywords='" + keywords + '\'' +
                ", containsGeodata=" + containsGeodata +
                ", containsAccEnvData=" + containsAccEnvData +
                ", isForeign=" + isForeign +
                ", isSeasonal=" + isSeasonal +
                ", season='" + season + '\'' +
                ", isArchive=" + isArchive +
                ", isNew=" + isNew +
                ", lastUpdateDate='" + lastUpdateDate + '\'' +
                ", sefUrl='" + sefUrl + '\'' +
                ", identificationNumber='" + identificationNumber + '\'' +
                '}';
    }
}