package boostbrain;


import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import org.jsoup.nodes.Document;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.*;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;


@Named
@SessionScoped
public class ReceivedDataBeen implements Serializable{

    private String dataString;
    private Document data;
    private ArrayList<DataFromApiMos> dataFromApiMos;

    public ArrayList<DataFromApiMos> getDataFromApiMos() {
        return dataFromApiMos;
    }

    public void setDataFromApiMos(ArrayList<DataFromApiMos> dataFromApiMos) {
        this.dataFromApiMos = dataFromApiMos;
    }

    public String getDataString() {
        return dataString;
    }

    public void setDataString(String dataString) {
        this.dataString = dataString;
    }

    public Document getData() {
        return data;
    }

    public void setData(Document data) {
        this.data = data;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReceivedDataBeen that = (ReceivedDataBeen) o;
        return Objects.equals(dataString, that.dataString) &&
                Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dataString, data);
    }

    @Override
    public String toString() {
        return "ReceivedDataBeen{" +
                "dataString='" + dataString + '\'' +
                ", data=" + data +
                '}';
    }

    public ArrayList<DataFromApiMos> sendPostHttpData() throws IOException {
//        URL url = new URL("https://apidata.mos.ru/v1/datasets?api_key=f9e80a1294732d9ecd33af822c6ad7af");
        URL url = new URL("https://apidata.mos.ru/v1/datasets?$top=30&api_key=f9e80a1294732d9ecd33af822c6ad7af");
        Map<String,Object> params = new LinkedHashMap<>();
//        params.put("$top", "1");
//        params.put("api_key", "f9e80a1294732d9ecd33af822c6ad7af");

        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");

        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);


        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

        StringBuilder sb = new StringBuilder();
        for (int c; (c = in.read()) >= 0;)
            sb.append((char)c);
        dataString = sb.toString();
        Reader reader = new StringReader(dataString);

//        Type collectionType = new TypeToken<Collection<DataFromApiMos>>(){}.getType();
//        Collection<DataFromApiMos> enums = gson.fromJson(reader, collectionType);

        try {
            Type collectionType = new TypeToken<ArrayList<DataFromApiMos>>(){}.getType();
            dataFromApiMos = new Gson().fromJson( reader , collectionType);
        } catch (JsonIOException e) {
            e.printStackTrace();
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return dataFromApiMos;
    }

}
