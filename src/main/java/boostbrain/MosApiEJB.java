package boostbrain;

import model.DataApiMosEntity;
import org.apache.commons.lang3.StringUtils;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;


@Stateless
public class MosApiEJB {

    @PersistenceContext(unitName = "examplePU")
    private EntityManager entityManager;



    public boolean setData(Integer id, Integer versionNumber, Integer releaseNumber, String caption, Integer categoryId, Integer departmentId,
                           String publishDate, String fullDescription, String keywords, Boolean containsGeodata,
                           Boolean containsAccEnvData, Boolean isForeign, Boolean isSeasonal, String season, Boolean isArchive,
                           Boolean isNew, String lastUpdateDate, String sefUrl, String identificationNumber) {

        DataApiMosEntity dataApiMosEntity = new DataApiMosEntity();
        dataApiMosEntity.setId(id);
        dataApiMosEntity.setVersionNumber(versionNumber);
        dataApiMosEntity.setReleaseNumber(releaseNumber);
        dataApiMosEntity.setCaption(caption);
        dataApiMosEntity.setCategoryId(categoryId);
        dataApiMosEntity.setDepartmentId(departmentId);
        dataApiMosEntity.setPublishDate(publishDate);
        dataApiMosEntity.setFullDescription(fullDescription);
        dataApiMosEntity.setKeywords(keywords);
        dataApiMosEntity.setContainsGeodata(containsGeodata);
        dataApiMosEntity.setContainsAccEnvData(containsAccEnvData);
        dataApiMosEntity.setForeign(isForeign);
        dataApiMosEntity.setSeasonal(isSeasonal);
        dataApiMosEntity.setSeason(season);
        dataApiMosEntity.setArchive(isArchive);
        dataApiMosEntity.setNew(isNew);
        dataApiMosEntity.setLastUpdateDate(lastUpdateDate);
        dataApiMosEntity.setSefUrl(sefUrl);
        dataApiMosEntity.setIdentificationNumber(identificationNumber);
        entityManager.persist(dataApiMosEntity);
        return true;
    }


    public List<DataApiMosEntity> getMosData(){

    Query query = entityManager.createQuery("select eMos from DataApiMosEntity eMos WHERE eMos.categoryId LIKE '5'");

    return query.getResultList();
}
}
